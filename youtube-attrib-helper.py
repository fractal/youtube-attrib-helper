from __future__ import unicode_literals
from wand.color import Color
from wand.font import Font
from wand.image import Image
import ffmpeg, getopt, json, os, sys, youtube_dl

# https://gist.github.com/jmilldotdev/b107f858729064daa940057fc9b14e89
class FilenameCollectorPP(youtube_dl.postprocessor.common.PostProcessor):
    def __init__(self):
        super(FilenameCollectorPP, self).__init__(None)
        self.filenames = []

    def run(self, information):
        self.filenames.append(information["filepath"])
        return [], information

# Catch and print youtube-dl errors
class YoutubeDlLogger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        print(msg)

    def error(self, msg):
        print(msg)


# Show download progress for youtube-dl
def youtubedl_hook(d):
    if d['status'] == 'downloading':
        progress = round(float(d['downloaded_bytes'])/float(d['total_bytes'])*100)
        if progress%5 == 0:
            print ("Downloading: "+ str(progress)+"%")
    if d['status'] == 'finished':
        print('Done downloading, now converting to MP3...')


# By default, place generated video in './output'.
output_dir = 'output' 
# By default, keep videos downloaded froml Youtube in './downloads'.
download_dir = 'downloads' 
# By default, create the attribution string from the video's metadata.
custom_attribution = None

# Script parameters handling
try:
    long_options = ["download-directory=", "output-directory=", "custom-attribution="]
    values = []
    arguments, values = getopt.getopt(sys.argv[1:], "", long_options)
     
    for currentArgument, currentValue in arguments:
        if currentArgument == "--download-directory":
            # Where the original Youtube video will be kept
            download_dir = currentValue
        elif currentArgument == "--output-directory":
            # Where the final video will be saved
            output_dir = currentValue
        elif currentArgument == "--custom-attribution":
            # Custom attribution string. If none provided, construct from the video metadata.
            custom_attribution = currentValue             
except getopt.error as err:
    print(str(err))

if len(sys.argv) == 1 or len(values) == 0:
    print("Usage: {} [--download-directory DIRECTORY] [--output-directory DIRECTORY] [--custom-attribution ATTRIBUTION] URL".format(sys.argv[0]))
    exit(1)
else:
    download_url = values[0]

for directory in [output_dir, download_dir]:
    if os.path.isdir(directory) == False or os.access(directory, os.W_OK) == False:
        raise Exception("Directory {} doesn't exist, or is not writable.".format(directory))


ydl_opts = {
    # Download the video with the best audio from Youtube.
    # Sometimes, it's not even a video, just an audio file.
    'format': 'bestaudio/best',
    # Do not delete the Youtube video after converting to MP3.
    # On subsequent runs with the same URL, the video will not have to be downloaded again.
    'keepvideo': True,
    # CLI safe filenames (e.g. no space in filename).
    'restrictfilenames': True,
    'postprocessors': [{
        # convert video to MP3 with Ffmpeg
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        'preferredquality': '192',
    }],
    'logger': YoutubeDlLogger(),
    'progress_hooks': [youtubedl_hook],
    # Where to save, and how to name the MP3 file.
    'outtmpl': '{}/%(title)s-%(id)s.%(ext)s'.format(download_dir)
}

with youtube_dl.YoutubeDL(ydl_opts) as ydl:
    # Adding this custom postprocessor is the only way to retrieve
    # the name of the MP3 file later.
    filename_collector = FilenameCollectorPP()
    ydl.add_post_processor(filename_collector)
    try:
        print("Downloading video from Youtube. This can take a while ...")
        ydl.download([download_url])
        print("Extracting metadata...")
        info_dict = ydl.extract_info(download_url, download=False)
    except:
        print("Error while fetching video or metadata!")
        raise

    # The file name of the MP3 which was just created.
    output_mp3_filename = filename_collector.filenames[0]
    
    if os.path.isfile(output_mp3_filename) == False or os.access(output_mp3_filename, os.R_OK) == False:
        raise Exception("The MP3 file does not exist or is not readable!")

    # Set aside the metadata we need.
    curated_dict = {
        "url": 'https://youtu.be/{}'.format(info_dict.get("id", None)),
        "title": info_dict.get('title', None),
        "channel": info_dict.get('channel', None),
        "license": info_dict.get('license', None),
        "tags": info_dict.get('tags', None)
    }

# File name for the image we're going to create.
output_png_filename = output_mp3_filename + '.png'

# Join all tags in a single string.
tag_string = "#" + " #".join(curated_dict['tags'])


# Prepare the text and its placement on the image.
if custom_attribution != None:
    # If the custom-attribution parameter was set,
    # use its value for the text on the image.
    text_parameters = [
        {
            'text': custom_attribution,
            'font': 'Poppins-SemiBold',
            'color': 'white',
            'top': 16,
            'height' : 400
        }
    ]
else :
    # If the custom-attribution parameter was not set,
    # use the original video's metadata for the text on the image.
    text_parameters = [
        {
            'text': curated_dict["title"],
            'font': 'Poppins-SemiBold',
            'color': 'white',
            'top': 24,
            'height': 155
        },
        {
            'text': curated_dict["channel"],
            'font': 'Poppins-SemiBold-Italic',
            'color': 'white',
            'top': 182,
            'height': 100
        },
        {
            'text': curated_dict["url"],
            'font': 'Poppins-SemiBold',
            'color': 'white',
            'top': 287,
            'height': 55
        },
        {
            'text': tag_string,
            'font': 'Poppins-SemiBold',
            'color': 'white',
            'top': 347,
            'height': 55
        }
    ]

print ("Creating image with attribution text...")

# Actually create the image.
try:
    with Image(width=768, height=432, pseudo='canvas:red') as img:
        for text in text_parameters:
            color = Color(text['color'])
            font = Font(path=text['font'], color=color)
            img.caption(
                text=text['text'],
                gravity='center',
                font=font,
                left=32,
                top=text['top'],
                width=690,
                height=text['height']
            )

        img.save(filename=output_png_filename)
        if os.path.isfile(output_png_filename) == False or os.access(output_png_filename, os.R_OK) == False:
            raise Exception("The PNG file does not exist or is not readable!")
except:
    print("Error while crafting or saving the image.")
    os.remove(output_mp3_filename)
    raise

print("Creating final video...")

try: 
    # Use the MP3 file we made as the audio for the new video.
    song = ffmpeg.input(output_mp3_filename)
    output_mp4_filename = output_dir+"/"+os.path.basename(output_mp3_filename).replace(".mp3", "-attrib.mp4")
    # Use the PNG image file we made as the only frame in the new video.
    attrib_image = ffmpeg.input(output_png_filename, **{'loop': 1, 'framerate': 1})
    (
        ffmpeg
        .output(song, attrib_image, output_mp4_filename, **{'codec:a': 'copy', 'codec:v': 'libx264', 'preset': 'fast', 'threads': 0, 'shortest': None, 'format': 'mp4' })
        .run(capture_stdout=True, overwrite_output=True)
    )
    if os.path.isfile(output_mp4_filename) == False or os.access(output_mp4_filename, os.R_OK) == False:
        raise Exception("The MP4 file does not exist or is not readable!")
except:
    print("Error while crafting or saving the video.")
    raise
finally:
    os.remove(output_mp3_filename)
    os.remove(output_png_filename)

print("")
print("Done. The video file is: {}".format(output_mp4_filename))