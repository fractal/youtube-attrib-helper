# youtube-attrib-helper

A lot of free royalty-free music is available on Youtube under a licence which requires only that you credit the authors if you use it in your project.
This script helps streamers make sure the correct attribution is displayed on screen when the song is playing.

See the image below to see how it works.

![How to use youtube-attrib-helper](./banner.png)

I personnaly use a "VLC Video source" in OBS, which plays all videos in a specific folder.

# Dependencies

- imagemagick
- ffmpeg
- python3
- The ["Poppins" font family](https://fonts.google.com/specimen/Poppins) (Open Font Licence)
- the following python libraries :
    - youtube-dl
    - Wand
    - ffmpeg-python

Has been tested on Ubuntu 21.10 only. The script might need slight (!) adjustments for other distributions, versions, and setups.



## Usage

```bash
python3 youtube-attrib-helper [--download-directory DIRECTORY] [--output-directory DIRECTORY] [--custom-attribution ATTRIBUTION] URL
```

### Options

#### --download-directory

Directory in which the original file downloaded from youtube will be kept.

The directory must exist before you run the script. The default is `./downloads`.

If you ever decide to run the script for the same URL again, youtube-dl will check in this directory if the file already exists. If it finds it, it will not need to download it again.
  

#### --output-directory

Directory where the new video  will be saved.

The directory must exist before you run the script. The default is `./output`.

#### --custom-attribution

Use this option to supply the string needed for attribution.

If the song's author does not require a specific string to be used, you can omit this parameter and a default attribution string will be built from the video's metadata found on Youtube.

The default attribution string includes the title, youtube channel, youtu.be short URL and tags.

### Examples

```bash

# Create a video with the default attribution string.
python3 youtube-attrib-helper.py https://www.youtube.com/watch?v=_cjwzqIBx_I

# Create a video with a custom attribution string.
python3 youtube-attrib-helper --custom-attribution "Music by Karl Casey @ White Bat Audio" https://www.youtube.com/watch?v=_cjwzqIBx_I
```

